# **Gesper** #

Gesper est une application Windows Form réalisée en deuxième année de BTS SIO.

## But de l'application ##

L'application permet de gérer les services, les diplômes et les employés.
On peut supprimer un service, un diplôme, un employé et mettre à jour tous les éléments qui dépendent de l'élément supprimé.
Ensuite on peut sauvegarder pour que les modifications soient modifiées dans la base de données.

### Diagramme de classes ###

![diagrammeDeClasses.png](https://bitbucket.org/repo/b4k4zy/images/3425458359-diagrammeDeClasses.png)

### Exemple d'utilisation ###

Fenêtre initial :

![fenetre1.png](https://bitbucket.org/repo/b4k4zy/images/4093480086-fenetre1.png)



Fenêtre Les Services :

![fenetre2.png](https://bitbucket.org/repo/b4k4zy/images/1452168235-fenetre2.png)