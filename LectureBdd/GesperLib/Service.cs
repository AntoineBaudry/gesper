﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace GesperLib
{
    public class Service
    {
        private int id;
        private decimal budget;
        private int capacite;
        private string designation;
        private string produit;
        private string type;
        private static int dernierId = 0;
        private List<Employe> lesEmployes = new List<Employe>();



        

       
        
        public Service(string p_designation)
        {
            dernierId++;
            lesEmployes = new List<Employe>();
        }

        public Service()
        {
            dernierId++;
            lesEmployes = new List<Employe>();
        }

        public Service(int id, string designation, string type, decimal budget)
        {
            this.id = id;
            this.designation = designation;
            this.type = type;
            this.budget = budget;
            dernierId = id;
            lesEmployes = new List<Employe>();
        }
        public Service(int id, string designation, string type, string produit, int capacite)
        {
            this.id = id;
            this.designation = designation;
            this.type = type;
            this.produit = produit;
            this.capacite = capacite;
            dernierId = id;
            lesEmployes = new List<Employe>();
        }

        public void PreparerSuppression()
        {
            foreach (Employe unEmploye in lesEmployes)
            {
                unEmploye.SupprimerService();
            }
        }

        #region accesseurs
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public decimal Budget
        {
            get { return budget; }
            set { budget = value; }
        }

        public int Capacite
        {
            get { return capacite; }
            set { capacite = value; }
        }

        public int DernierId
        {
            get { return dernierId; }
            set { dernierId = value; }
        }

        public string Designation
        {
            get { return designation; }
            set { designation = value; }
        }

        public string Produit
        {
            get { return produit; }
            set { produit = value; }
        }

        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        public List<Employe> LesEmployes
        {
            get { return lesEmployes; }
            set { lesEmployes = value; }
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5}", this.id, this.designation, this.type, this.produit,this.capacite, this.budget);
        }
        #endregion

    }
}
