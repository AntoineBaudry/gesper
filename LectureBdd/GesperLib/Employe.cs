﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace GesperLib
{
    public class Employe
    {
        private int id;
        private string nom;
        private string prenom;
        private decimal salaire;
        private string sexe;
        private byte cadre;
        private Service leService;
        private List<Diplome> lesDiplomes = new List<Diplome>();


        public Employe(int id, string nom, string prenom, string sexe, byte cadre, decimal salaire)
        {
            this.id = id;
            this.nom = nom;
            this.prenom = prenom;
            this.salaire = salaire;
            this.sexe = sexe;
            this.cadre = cadre;
        }

        public void AjouterDiplome(Diplome unDiplome)
        {
            lesDiplomes.Add(unDiplome);
            unDiplome.LesEmployes.Add(this);
        }
        public void SupprimerDiplome(Diplome unDiplome, bool majDiplome)
        {
            lesDiplomes.Remove(unDiplome);
            if (majDiplome)
            {
                unDiplome.LesEmployes.Remove(this);
            }
        }

        public void PreparerSuppression()
        {
            leService = null;
            foreach (Diplome unDiplome in lesDiplomes)
            {
                unDiplome.LesEmployes.Remove(this);
            }
        }

        public void SupprimerService()
        {
            this.leService = null;
        }

        #region accesseurs
        public List<Diplome> LesDiplomes
        {
            get { return lesDiplomes; }
            set { lesDiplomes = value; }
        }
        public Service LeService
        {
            get { return leService; }
            set {
                if (leService != null)
                {
                    leService.LesEmployes.Remove(this);
                }
                leService = value;
                if (leService != null)
                {
                    leService.LesEmployes.Add(this);
                }
            }
        }
        public byte Cadre
        {
            get { return cadre; }
            set { cadre = value; }
        }
        public string Sexe
        {
            get { return sexe; }
            set { sexe = value; }
        }
        public decimal Salaire
        {
            get { return salaire; }
            set { salaire = value; }
        }
        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }
        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5} {6}", this.id, this.nom, this.prenom, this.salaire, this.sexe, this.cadre,leService);
        }
        #endregion
    }
}
