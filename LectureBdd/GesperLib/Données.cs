﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace GesperLib
{
    public class Données
    {


        private List<Service> lesServices;
        private List<Employe> lesEmployes;
        private List<Diplome> lesDiplomes;

        public void AfficherDiplomes()
        {
            for (int i = 0; i < LesDiplomes.Count; i++)
            {
                Console.WriteLine(lesDiplomes[i].ToString());
            }
        }

        public void AfficherEmployes()
        {
            for (int i = 0; i < LesEmployes.Count; i++)
            {
                Console.WriteLine(lesEmployes[i].ToString());
            }
        }

        public void AfficherServices()
        {
            for (int i = 0; i < LesServices.Count; i++)
            {
                Console.WriteLine(lesServices[i].ToString());
            }
        }

        public void Charger()
        {

        }

        public void ChargerDiplomes(MySqlConnection Cnx)
        {
            string requête = "select * from diplome";
            MySqlCommand cmd = new MySqlCommand(requête, Cnx);
            MySqlDataReader RdR = cmd.ExecuteReader();
            while (RdR.Read())
            {
                Diplome nouveauDiplome = new Diplome(Convert.ToInt32(RdR["dip_id"]), Convert.ToString(RdR["dip_libelle"]));
                lesDiplomes.Add(nouveauDiplome);
            }
            RdR.Close();
        }

        public void ChargerEmploye(MySqlConnection Cnx)
        {           
            string id = null;
            string requête = "select * from employe";
            MySqlCommand cmd = new MySqlCommand(requête, Cnx);
            MySqlDataReader RdR = cmd.ExecuteReader();
            while (RdR.Read())
            {
                Employe nouveauEmploye = new Employe(Convert.ToInt32(RdR["emp_id"]), Convert.ToString(RdR["emp_nom"]), Convert.ToString(RdR["emp_prenom"]), Convert.ToString(RdR["emp_sexe"]), Convert.ToByte(RdR["emp_cadre"]), Convert.ToDecimal(RdR["emp_salaire"]));
                int i = 0;
                id = Convert.ToString(RdR["emp_service"]);
                if (id != "")
                {
                    while (i < lesServices.Count && lesServices[i].Id != Convert.ToInt32(RdR["emp_service"]))
                    {
                        i++;
                    }
                    if (lesServices[i].Id == Convert.ToInt32(RdR["emp_service"]))
                    {
                        nouveauEmploye.LeService = lesServices[i];
                    }
                }            
                this.lesEmployes.Add(nouveauEmploye);
            }
            RdR.Close();
        }

        public void ChargerLesDiplomesDesEmployes(MySqlConnection Cnx)
        {
            foreach (Employe leEmploye in lesEmployes)
            {
                string requête = "Select * from Diplome, posseder, employe where emp_id = pos_employe and pos_diplome = dip_id and emp_id =" + leEmploye.Id;
                MySqlCommand command = new MySqlCommand(requête, Cnx);
                MySqlDataReader Rdr = command.ExecuteReader();
                while (Rdr.Read())
                {
                    int i = 0;
                    while (i < lesDiplomes.Count && lesDiplomes[i].Id != Convert.ToInt32(Rdr["dip_id"]))
                    {
                        i++;
                    }
                    if (i < lesDiplomes.Count && lesDiplomes[i].Id == Convert.ToInt32(Rdr["dip_id"]))
                    {
                        leEmploye.LesDiplomes.Add(lesDiplomes[i]);
                    }
                }
                Rdr.Close();
            }
        }

        public void ChargerLesEmployesDesServices(MySqlConnection Cnx)
        {
            foreach (Service leService in lesServices)
            {
                string requête = "Select emp_id from employe where emp_service =" + leService.Id;
                MySqlCommand command = new MySqlCommand(requête, Cnx);
                MySqlDataReader Rdr = command.ExecuteReader();
                while (Rdr.Read())
                {
                    int i = 0;
                    while (i < lesEmployes.Count && lesEmployes[i].Id != Convert.ToInt32(Rdr["emp_id"]))
                    {
                        i++;
                    }
                    if (lesEmployes[i].Id == Convert.ToInt32(Rdr["emp_id"]))
                    {
                        leService.LesEmployes.Add(lesEmployes[i]);
                    }
                }
                Rdr.Close();
            }
        }

        public void ChargerLesEmployesTitulairesDesDiplomes(MySqlConnection Cnx)
        {
            foreach (Diplome leDiplome in lesDiplomes)
            {
                string requete = "Select * from Employe, Posseder, Diplome where emp_id=pos_employe and pos_diplome=dip_id and dip_id=" + leDiplome.Id;
                MySqlCommand command = new MySqlCommand(requete, Cnx);
                MySqlDataReader rdr = command.ExecuteReader();

                while (rdr.Read())
                {
                    int i = 0;
                    while (i < lesEmployes.Count && lesEmployes[i].Id != Convert.ToInt32(rdr["emp_id"]))
                    {
                        i++;
                    }
                    if (i < lesEmployes.Count && lesEmployes[i].Id == Convert.ToInt32(rdr["emp_id"]))
                    {
                        leDiplome.LesEmployes.Add(lesEmployes[i]);
                    }
                }
                rdr.Close();
            }
        }

        public void ChargerServices(MySqlConnection Cnx)
        {

            string requête = "select * from service";
            MySqlCommand cmd = new MySqlCommand(requête, Cnx);
            MySqlDataReader RdR = cmd.ExecuteReader();

            while (RdR.Read())
            {
                if (RdR["ser_type"].ToString() == "A")
                {
                    Service nouveauService = new Service(
                        Convert.ToInt32(RdR["ser_id"]),
                        Convert.ToString(RdR["ser_designation"]),
                        Convert.ToString(RdR["ser_type"]),
                        Convert.ToDecimal(RdR["ser_budget"]));
                    lesServices.Add(nouveauService);
                }

                if (RdR["ser_type"].ToString() == "P")
                {
                    Service nouveauService = new Service(
                        Convert.ToInt32(RdR["ser_id"]),
                        Convert.ToString(RdR["ser_designation"]),
                        Convert.ToString(RdR["ser_type"]),
                        Convert.ToString(RdR["ser_produit"]),
                        Convert.ToInt32(RdR["ser_capacite"]));
                    lesServices.Add(nouveauService);
                }
            }
            RdR.Close();
        }

        public MySqlConnection ConnexionBDD()
        {
            string Connexion = "SERVER=localhost; DATABASE=gesper; UID=root; PASSWORD=siojjr";
            MySqlConnection Cnx = new MySqlConnection(Connexion);
            try
            {
                Cnx.Open();
                Console.WriteLine("Connexion réussie");
            }
            catch (Exception e)
            {
                Console.WriteLine("Erreur connexion" + e.Message.ToString());
            }
            return Cnx;
        }

        public void DéconnexionBDD(MySqlConnection Cnx)
        {
            Cnx.Close();
        }

        public Données()
        {
            lesDiplomes = new List<Diplome>();
            lesEmployes = new List<Employe>();
            lesServices = new List<Service>();
        }

        public void Sauver(MySqlConnection Cnx)
        {

            #region viderBase
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = Cnx;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "viderBase";
            cmd.Prepare();
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            #endregion

            #region insertDiplome
            MySqlCommand cmd1 = new MySqlCommand();
            cmd1.Connection = Cnx;
            cmd1.CommandType = System.Data.CommandType.StoredProcedure;
            cmd1.CommandText = "insertDiplome";
            MySqlParameter d_libelle = new MySqlParameter("libelle", MySqlDbType.String);
            cmd1.Parameters.Add(d_libelle);


            foreach (Diplome d in lesDiplomes)
            {
                d_libelle.Value = d.Libelle;
                cmd1.Prepare();
                try
                {
                    cmd1.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            #endregion

            #region insertService
            MySqlCommand cmdInsertServices = new MySqlCommand();
            cmdInsertServices.Connection = Cnx;
            cmdInsertServices.CommandType = System.Data.CommandType.StoredProcedure;
            cmdInsertServices.CommandText = "insertService";
            MySqlParameter s_id = new MySqlParameter("id", MySqlDbType.Int32);
            MySqlParameter s_designation = new MySqlParameter("designation", MySqlDbType.String);
            MySqlParameter s_type = new MySqlParameter("serType", MySqlDbType.String);
            MySqlParameter s_produit = new MySqlParameter("produit", MySqlDbType.String);
            MySqlParameter s_capacite = new MySqlParameter("capacite", MySqlDbType.Int32);
            MySqlParameter s_budget = new MySqlParameter("budget", MySqlDbType.Decimal);
            cmdInsertServices.Parameters.Add(s_id);
            cmdInsertServices.Parameters.Add(s_designation);
            cmdInsertServices.Parameters.Add(s_type);
            cmdInsertServices.Parameters.Add(s_produit);
            cmdInsertServices.Parameters.Add(s_capacite);
            cmdInsertServices.Parameters.Add(s_budget);
            foreach (Service unService in lesServices)
            {
                s_id.Value = unService.Id;
                s_designation.Value = unService.Designation;
                s_type.Value = unService.Type;
                s_produit.Value = unService.Produit;
                s_capacite.Value = unService.Capacite;
                s_budget.Value = unService.Budget;
                cmdInsertServices.Prepare();
                try
                {
                    cmdInsertServices.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            #endregion

            #region insertEmploye
            MySqlCommand cmd2 = new MySqlCommand();
            cmd2.Connection = Cnx;
            cmd2.CommandType = System.Data.CommandType.StoredProcedure;
            cmd2.CommandText = "insertEmploye";
            MySqlParameter e_nom = new MySqlParameter("nom", MySqlDbType.String);
            cmd2.Parameters.Add(e_nom);

            MySqlParameter e_prenom = new MySqlParameter("prenom", MySqlDbType.String);
            cmd2.Parameters.Add(e_prenom);

            MySqlParameter e_sexe = new MySqlParameter("sexe", MySqlDbType.String);
            cmd2.Parameters.Add(e_sexe);

            MySqlParameter e_cadre = new MySqlParameter("cadre", MySqlDbType.Bit);
            cmd2.Parameters.Add(e_cadre);

            MySqlParameter e_salaire = new MySqlParameter("salaire", MySqlDbType.Decimal);
            cmd2.Parameters.Add(e_salaire);

            MySqlParameter e_service = new MySqlParameter("service", MySqlDbType.Int32);
            cmd2.Parameters.Add(e_service);
            foreach (Employe unEmploye in lesEmployes)
            {
                e_nom.Value = unEmploye.Nom;
                e_prenom.Value = unEmploye.Prenom;
                e_sexe.Value = unEmploye.Sexe;
                e_cadre.Value = unEmploye.Cadre;
                e_salaire.Value = unEmploye.Salaire;
                if (unEmploye.LeService != null)
                {
                    e_service.Value = unEmploye.LeService.Id;
                }
                else
                {
                    e_service.Value = null;
                }
               
                cmd2.Prepare();
                try
                {
                    cmd2.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            #endregion

            #region insertPosseder
            MySqlCommand cmdInsertPosseder = new MySqlCommand();
            cmdInsertPosseder.Connection = Cnx;
            cmdInsertPosseder.CommandType = System.Data.CommandType.StoredProcedure;
            cmdInsertPosseder.CommandText = "insertPosseder";

            MySqlParameter p_diplome = new MySqlParameter("diplome", MySqlDbType.Int32);
            cmdInsertPosseder.Parameters.Add(p_diplome);

            MySqlParameter p_employe = new MySqlParameter("employe", MySqlDbType.Int32);
            cmdInsertPosseder.Parameters.Add(p_employe);
            foreach (Employe unEmploye in lesEmployes)
            {
                for (int i = 0; i < unEmploye.LesDiplomes.Count; i++)
                {
                    p_diplome.Value = unEmploye.LesDiplomes[i].Id;
                    p_employe.Value = unEmploye.Id;
                    cmdInsertPosseder.Prepare();
                    try
                    {
                        cmdInsertPosseder.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }

            try
            {
                cmdInsertPosseder.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            #endregion
        }


        public void ToutCharger(MySqlConnection Cnx)
        {
            ChargerDiplomes(Cnx);
            ChargerServices(Cnx);
            ChargerEmploye(Cnx);
            ChargerLesDiplomesDesEmployes(Cnx);
            ChargerLesEmployesDesServices(Cnx);
            ChargerLesEmployesTitulairesDesDiplomes(Cnx);
        }


        #region Accesseurs
        public List<Service> LesServices
        {
            get { return lesServices; }
            set { lesServices = value; }
        }
        public List<Employe> LesEmployes
        {
            get { return lesEmployes; }
            set { lesEmployes = value; }
        }
        public List<Diplome> LesDiplomes
        {
            get { return lesDiplomes; }
            set { lesDiplomes = value; }
        }
        #endregion
    }
}

