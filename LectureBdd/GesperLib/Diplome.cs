﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace GesperLib
{
    public class Diplome
    {
        private int id;
        private string libelle;
        private ArrayList lesEmployes = new ArrayList();


        public void PreparerSuppression()
        {
            foreach (Employe unEmploye in lesEmployes)
            {
                unEmploye.SupprimerDiplome(this, false);
            }
        }

        #region accesseurs
        public override string ToString()
        {
            return string.Format("{0} {1} ",this.id, this.libelle);
        }
        public ArrayList LesEmployes
        {
            get { return lesEmployes; }
            set { lesEmployes = value; }
        }
        public Diplome(int id, string libelle)
        {
            this.id = id;
            this.libelle = libelle;
        }
        public string Libelle
        {
            get { return libelle; }
            set { libelle = value; }
        }
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        #endregion
    }
}
