﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GesperLib;
using MySql.Data.MySqlClient;

namespace WF_Gesper
{
    public partial class fm_Employes : Form
    {
        private Données p_bd;
        public fm_Employes(Données bd, MySqlConnection Cnx)
        {
            p_bd = bd;
            InitializeComponent();

            tbId.Enabled = false;
            tbId.ReadOnly = true;
            bsEmployes.DataSource = bd.LesEmployes;
            bsDiplomes.DataSource = bd.LesDiplomes;
            tbId.DataBindings.Add("Text", bsEmployes, "Id");
            tbNom.DataBindings.Add("Text", bsEmployes, "nom");
            tbPrenom.DataBindings.Add("Text", bsEmployes, "prenom");
            cbCadre.DataBindings.Add("checkState", bsEmployes, "Cadre", true);
            tbSalaire.DataBindings.Add("Text", bsEmployes, "Salaire");
            bnEmployés.BindingSource = bsEmployes;
            //lbDiplomesPossedes.DataSource = bsDiplomes;
            for (int i = 0; i < bd.LesServices.Count; i++)
            {
                cbService.Items.Add(bd.LesServices[i].Designation);
            }
            if (bsEmployes.Current != null)
            {
                Employe unEmploye = (Employe)bsEmployes.Current;
                for (int i = 0; i < cbService.Items.Count; i++)
                {
                    if (unEmploye.LeService != null)
                    {
                        if ((string)cbService.Items[i] == unEmploye.LeService.Designation)
                        {
                            cbService.SelectedItem = cbService.Items[i];
                        }
                    }
                    else
                    {
                        cbService.SelectedItem = cbService.Items[0];
                    }
                }
            }
        }

        private void bsEmployes_CurrentChanged(object sender, EventArgs e)
        {
            if (bsEmployes.Current != null)
            {
                bsDiplomes.DataSource = ((Employe)bsEmployes.Current).LesDiplomes;
                Employe unEmploye = (Employe)bsEmployes.Current;
                for (int i = 0; i < cbService.Items.Count; i++)
                {
                    if (unEmploye.LeService != null)
                    {
                        if ((string)cbService.Items[i] == unEmploye.LeService.Designation)
                        {
                            cbService.SelectedItem = cbService.Items[i];
                        }
                    }
                    else
                    {
                        cbService.SelectedItem = cbService.Items[0];
                    }
                }
                if (unEmploye.Sexe == "F")
                {
                    rbFéminin.Checked = true;
                }
                else
                {
                    rbMasculin.Checked = true;
                }
            }
        }

        private void rbSexe_Click(object sender, EventArgs e)
        {
            if (bsEmployes.Current != null)
            {
                Employe unEmploye = (Employe)bsEmployes.Current;
                if (rbFéminin.Checked)
                {
                    unEmploye.Sexe = "F";
                }
                else if (rbMasculin.Checked)
                {
                    unEmploye.Sexe = "M";
                }
            }
        }

        private void btnVersAutreDiplômes_Click(object sender, EventArgs e)
        {

        }

        private void btnVersDiplômesPossédés_Click(object sender, EventArgs e)
        {

        }

    }
}
