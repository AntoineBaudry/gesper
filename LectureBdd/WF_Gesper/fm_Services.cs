﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using GesperLib;

namespace WF_Gesper
{
    public partial class fm_Services : Form
    {
        private Données p_bd;
        public fm_Services(Données bd, MySqlConnection Cnx)
        {
            InitializeComponent();
            p_bd = bd;
            tbId.ReadOnly = true;
            tbId.Enabled = false;
            bsServices.DataSource = bd.LesServices;
            tbId.DataBindings.Add("Text", bsServices, "Id");
            tbDesignation.DataBindings.Add("Text", bsServices, "Designation");
            tbType.DataBindings.Add("Text", bsServices, "Type");
            tbProduit.DataBindings.Add("Text", bsServices, "Produit");
            tbCapacite.DataBindings.Add("Text", bsServices, "Capacite");
            tbBudget.DataBindings.Add("Text", bsServices, "Budget");
            bnServices.BindingSource = bsServices;
            lbLesEmployes.DataSource = bsEmployes;
        }

        private void bsServices_CurrentChanged(object sender, EventArgs e)
        {
            if (bsServices.Current != null)
            {
                bsEmployes.DataSource = ((Service)bsServices.Current).LesEmployes;
            }
            else
            {
                bsEmployes.DataSource = null;
            }
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (bsServices.Current != null)
            {
                bnServices.BeginInit();
                Service leService = (Service)bsServices.Current;
                leService.PreparerSuppression();
                bnServices.EndInit();
                p_bd.LesServices.RemoveAt(Convert.ToInt32(bnServices.PositionItem.Text)-1);
                bsServices.MoveNext();
                bsServices.MovePrevious();
            }
        }
    }
}
