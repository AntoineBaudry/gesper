﻿namespace WF_Gesper
{
    partial class fm_Employes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fm_Employes));
            this.lblId = new System.Windows.Forms.Label();
            this.lblNom = new System.Windows.Forms.Label();
            this.lblPrenom = new System.Windows.Forms.Label();
            this.lblSalaire = new System.Windows.Forms.Label();
            this.lblService = new System.Windows.Forms.Label();
            this.tbId = new System.Windows.Forms.TextBox();
            this.lblDiplomesPossédés = new System.Windows.Forms.Label();
            this.lblAutresDiplomes = new System.Windows.Forms.Label();
            this.tbNom = new System.Windows.Forms.TextBox();
            this.tbPrenom = new System.Windows.Forms.TextBox();
            this.tbSalaire = new System.Windows.Forms.TextBox();
            this.cbService = new System.Windows.Forms.ComboBox();
            this.cbCadre = new System.Windows.Forms.CheckBox();
            this.gpSexe = new System.Windows.Forms.GroupBox();
            this.rbMasculin = new System.Windows.Forms.RadioButton();
            this.rbFéminin = new System.Windows.Forms.RadioButton();
            this.lbDiplomesPossedes = new System.Windows.Forms.ListBox();
            this.lbAutreDiplômes = new System.Windows.Forms.ListBox();
            this.btnVersAutreDiplômes = new System.Windows.Forms.Button();
            this.btnVersDiplômesPossédés = new System.Windows.Forms.Button();
            this.bnEmployés = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bsEmployes = new System.Windows.Forms.BindingSource(this.components);
            this.bsDiplomes = new System.Windows.Forms.BindingSource(this.components);
            this.gpSexe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnEmployés)).BeginInit();
            this.bnEmployés.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsEmployes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDiplomes)).BeginInit();
            this.SuspendLayout();
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(17, 35);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(19, 13);
            this.lblId.TabIndex = 0;
            this.lblId.Text = "Id:";
            // 
            // lblNom
            // 
            this.lblNom.AutoSize = true;
            this.lblNom.Location = new System.Drawing.Point(17, 61);
            this.lblNom.Name = "lblNom";
            this.lblNom.Size = new System.Drawing.Size(32, 13);
            this.lblNom.TabIndex = 1;
            this.lblNom.Text = "Nom:";
            // 
            // lblPrenom
            // 
            this.lblPrenom.AutoSize = true;
            this.lblPrenom.Location = new System.Drawing.Point(17, 87);
            this.lblPrenom.Name = "lblPrenom";
            this.lblPrenom.Size = new System.Drawing.Size(46, 13);
            this.lblPrenom.TabIndex = 2;
            this.lblPrenom.Text = "Prénom:";
            // 
            // lblSalaire
            // 
            this.lblSalaire.AutoSize = true;
            this.lblSalaire.Location = new System.Drawing.Point(17, 136);
            this.lblSalaire.Name = "lblSalaire";
            this.lblSalaire.Size = new System.Drawing.Size(42, 13);
            this.lblSalaire.TabIndex = 3;
            this.lblSalaire.Text = "Salaire:";
            // 
            // lblService
            // 
            this.lblService.AutoSize = true;
            this.lblService.Location = new System.Drawing.Point(17, 212);
            this.lblService.Name = "lblService";
            this.lblService.Size = new System.Drawing.Size(46, 13);
            this.lblService.TabIndex = 4;
            this.lblService.Text = "Service:";
            // 
            // tbId
            // 
            this.tbId.Location = new System.Drawing.Point(69, 32);
            this.tbId.Name = "tbId";
            this.tbId.Size = new System.Drawing.Size(100, 20);
            this.tbId.TabIndex = 5;
            // 
            // lblDiplomesPossédés
            // 
            this.lblDiplomesPossédés.AutoSize = true;
            this.lblDiplomesPossédés.Location = new System.Drawing.Point(17, 243);
            this.lblDiplomesPossédés.Name = "lblDiplomesPossédés";
            this.lblDiplomesPossédés.Size = new System.Drawing.Size(101, 13);
            this.lblDiplomesPossédés.TabIndex = 6;
            this.lblDiplomesPossédés.Text = "Diplômes possédés:";
            // 
            // lblAutresDiplomes
            // 
            this.lblAutresDiplomes.AutoSize = true;
            this.lblAutresDiplomes.Location = new System.Drawing.Point(210, 243);
            this.lblAutresDiplomes.Name = "lblAutresDiplomes";
            this.lblAutresDiplomes.Size = new System.Drawing.Size(86, 13);
            this.lblAutresDiplomes.TabIndex = 7;
            this.lblAutresDiplomes.Text = "Autres Diplômes:";
            // 
            // tbNom
            // 
            this.tbNom.Location = new System.Drawing.Point(69, 58);
            this.tbNom.Name = "tbNom";
            this.tbNom.Size = new System.Drawing.Size(100, 20);
            this.tbNom.TabIndex = 8;
            // 
            // tbPrenom
            // 
            this.tbPrenom.Location = new System.Drawing.Point(69, 84);
            this.tbPrenom.Name = "tbPrenom";
            this.tbPrenom.Size = new System.Drawing.Size(100, 20);
            this.tbPrenom.TabIndex = 9;
            // 
            // tbSalaire
            // 
            this.tbSalaire.Location = new System.Drawing.Point(68, 133);
            this.tbSalaire.Name = "tbSalaire";
            this.tbSalaire.Size = new System.Drawing.Size(100, 20);
            this.tbSalaire.TabIndex = 10;
            // 
            // cbService
            // 
            this.cbService.FormattingEnabled = true;
            this.cbService.Location = new System.Drawing.Point(68, 209);
            this.cbService.Name = "cbService";
            this.cbService.Size = new System.Drawing.Size(121, 21);
            this.cbService.TabIndex = 11;
            // 
            // cbCadre
            // 
            this.cbCadre.AutoSize = true;
            this.cbCadre.Location = new System.Drawing.Point(69, 110);
            this.cbCadre.Name = "cbCadre";
            this.cbCadre.Size = new System.Drawing.Size(54, 17);
            this.cbCadre.TabIndex = 12;
            this.cbCadre.Text = "Cadre";
            this.cbCadre.UseVisualStyleBackColor = true;
            // 
            // gpSexe
            // 
            this.gpSexe.Controls.Add(this.rbMasculin);
            this.gpSexe.Controls.Add(this.rbFéminin);
            this.gpSexe.Location = new System.Drawing.Point(20, 159);
            this.gpSexe.Name = "gpSexe";
            this.gpSexe.Size = new System.Drawing.Size(151, 44);
            this.gpSexe.TabIndex = 13;
            this.gpSexe.TabStop = false;
            this.gpSexe.Text = "Sexe:";
            // 
            // rbMasculin
            // 
            this.rbMasculin.AutoSize = true;
            this.rbMasculin.Location = new System.Drawing.Point(73, 19);
            this.rbMasculin.Name = "rbMasculin";
            this.rbMasculin.Size = new System.Drawing.Size(67, 17);
            this.rbMasculin.TabIndex = 1;
            this.rbMasculin.TabStop = true;
            this.rbMasculin.Text = "Masculin";
            this.rbMasculin.UseVisualStyleBackColor = true;
            this.rbMasculin.Click += new System.EventHandler(this.rbSexe_Click);
            // 
            // rbFéminin
            // 
            this.rbFéminin.AutoSize = true;
            this.rbFéminin.Location = new System.Drawing.Point(6, 19);
            this.rbFéminin.Name = "rbFéminin";
            this.rbFéminin.Size = new System.Drawing.Size(61, 17);
            this.rbFéminin.TabIndex = 0;
            this.rbFéminin.TabStop = true;
            this.rbFéminin.Text = "Féminin";
            this.rbFéminin.UseVisualStyleBackColor = true;
            this.rbFéminin.Click += new System.EventHandler(this.rbSexe_Click);
            // 
            // lbDiplomesPossedes
            // 
            this.lbDiplomesPossedes.FormattingEnabled = true;
            this.lbDiplomesPossedes.Location = new System.Drawing.Point(20, 259);
            this.lbDiplomesPossedes.Name = "lbDiplomesPossedes";
            this.lbDiplomesPossedes.Size = new System.Drawing.Size(120, 95);
            this.lbDiplomesPossedes.TabIndex = 14;
            // 
            // lbAutreDiplômes
            // 
            this.lbAutreDiplômes.FormattingEnabled = true;
            this.lbAutreDiplômes.Location = new System.Drawing.Point(213, 259);
            this.lbAutreDiplômes.Name = "lbAutreDiplômes";
            this.lbAutreDiplômes.Size = new System.Drawing.Size(120, 95);
            this.lbAutreDiplômes.TabIndex = 15;
            // 
            // btnVersAutreDiplômes
            // 
            this.btnVersAutreDiplômes.Location = new System.Drawing.Point(146, 268);
            this.btnVersAutreDiplômes.Name = "btnVersAutreDiplômes";
            this.btnVersAutreDiplômes.Size = new System.Drawing.Size(61, 23);
            this.btnVersAutreDiplômes.TabIndex = 16;
            this.btnVersAutreDiplômes.Text = ">";
            this.btnVersAutreDiplômes.UseVisualStyleBackColor = true;
            this.btnVersAutreDiplômes.Click += new System.EventHandler(this.btnVersAutreDiplômes_Click);
            // 
            // btnVersDiplômesPossédés
            // 
            this.btnVersDiplômesPossédés.Location = new System.Drawing.Point(146, 318);
            this.btnVersDiplômesPossédés.Name = "btnVersDiplômesPossédés";
            this.btnVersDiplômesPossédés.Size = new System.Drawing.Size(61, 23);
            this.btnVersDiplômesPossédés.TabIndex = 17;
            this.btnVersDiplômesPossédés.Text = "<";
            this.btnVersDiplômesPossédés.UseVisualStyleBackColor = true;
            this.btnVersDiplômesPossédés.Click += new System.EventHandler(this.btnVersDiplômesPossédés_Click);
            // 
            // bnEmployés
            // 
            this.bnEmployés.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bnEmployés.CountItem = this.bindingNavigatorCountItem;
            this.bnEmployés.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bnEmployés.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem});
            this.bnEmployés.Location = new System.Drawing.Point(0, 0);
            this.bnEmployés.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bnEmployés.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bnEmployés.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bnEmployés.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bnEmployés.Name = "bnEmployés";
            this.bnEmployés.PositionItem = this.bindingNavigatorPositionItem;
            this.bnEmployés.Size = new System.Drawing.Size(454, 25);
            this.bnEmployés.TabIndex = 18;
            this.bnEmployés.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Ajouter nouveau";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(37, 22);
            this.bindingNavigatorCountItem.Text = "de {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Nombre total d\'éléments";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Supprimer";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Placer en premier";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Déplacer vers le haut";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Position actuelle";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Déplacer vers le bas";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Placer en dernier";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bsEmployes
            // 
            this.bsEmployes.CurrentChanged += new System.EventHandler(this.bsEmployes_CurrentChanged);
            // 
            // fm_Employes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 366);
            this.Controls.Add(this.bnEmployés);
            this.Controls.Add(this.btnVersDiplômesPossédés);
            this.Controls.Add(this.btnVersAutreDiplômes);
            this.Controls.Add(this.lbAutreDiplômes);
            this.Controls.Add(this.lbDiplomesPossedes);
            this.Controls.Add(this.gpSexe);
            this.Controls.Add(this.cbCadre);
            this.Controls.Add(this.cbService);
            this.Controls.Add(this.tbSalaire);
            this.Controls.Add(this.tbPrenom);
            this.Controls.Add(this.tbNom);
            this.Controls.Add(this.lblAutresDiplomes);
            this.Controls.Add(this.lblDiplomesPossédés);
            this.Controls.Add(this.tbId);
            this.Controls.Add(this.lblService);
            this.Controls.Add(this.lblSalaire);
            this.Controls.Add(this.lblPrenom);
            this.Controls.Add(this.lblNom);
            this.Controls.Add(this.lblId);
            this.Name = "fm_Employes";
            this.Text = "Employes";
            this.gpSexe.ResumeLayout(false);
            this.gpSexe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnEmployés)).EndInit();
            this.bnEmployés.ResumeLayout(false);
            this.bnEmployés.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsEmployes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDiplomes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label lblNom;
        private System.Windows.Forms.Label lblPrenom;
        private System.Windows.Forms.Label lblSalaire;
        private System.Windows.Forms.Label lblService;
        private System.Windows.Forms.TextBox tbId;
        private System.Windows.Forms.Label lblDiplomesPossédés;
        private System.Windows.Forms.Label lblAutresDiplomes;
        private System.Windows.Forms.TextBox tbNom;
        private System.Windows.Forms.TextBox tbPrenom;
        private System.Windows.Forms.TextBox tbSalaire;
        private System.Windows.Forms.ComboBox cbService;
        private System.Windows.Forms.CheckBox cbCadre;
        private System.Windows.Forms.GroupBox gpSexe;
        private System.Windows.Forms.RadioButton rbMasculin;
        private System.Windows.Forms.RadioButton rbFéminin;
        private System.Windows.Forms.ListBox lbDiplomesPossedes;
        private System.Windows.Forms.ListBox lbAutreDiplômes;
        private System.Windows.Forms.Button btnVersAutreDiplômes;
        private System.Windows.Forms.Button btnVersDiplômesPossédés;
        private System.Windows.Forms.BindingSource bsEmployes;
        private System.Windows.Forms.BindingSource bsDiplomes;
        private System.Windows.Forms.BindingNavigator bnEmployés;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
    }
}