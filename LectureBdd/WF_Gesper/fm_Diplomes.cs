﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using GesperLib;

namespace WF_Gesper
{
    public partial class fm_Diplomes : Form
    {
        private Données p_bd;
        public fm_Diplomes(Données bd, MySqlConnection Cnx)
        {
            InitializeComponent();
            bsDiplomes.CurrentChanged += bsDiplomes_CurrentChanged;

            this.p_bd = bd;
            tbId.ReadOnly = true;
            tbId.Enabled = false;
            tbLibelle.ReadOnly = true;
            tbLibelle.Enabled = false;
            bsDiplomes.DataSource = bd.LesDiplomes;
            tbId.DataBindings.Add("Text", bsDiplomes, "Id");
            tbLibelle.DataBindings.Add("Text", bsDiplomes, "Libelle");
            bnDiplomes.BindingSource = bsDiplomes;
            lbEmployés.DataSource = bsEmployes;

            
        }

        private void bsDiplomes_CurrentChanged(object sender, EventArgs e)
        {
            if (bsDiplomes.Current != null)
            {
                bsEmployes.DataSource = ((Diplome)bsDiplomes.Current).LesEmployes ;
            }
            else
            {
                bsEmployes.DataSource = null;
            }
        }
    }
}
