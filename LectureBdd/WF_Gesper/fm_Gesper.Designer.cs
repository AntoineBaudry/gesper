﻿namespace WF_Gesper
{
    partial class fm_Gesper
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnServices = new System.Windows.Forms.Button();
            this.btnDiplomes = new System.Windows.Forms.Button();
            this.btnEmployes = new System.Windows.Forms.Button();
            this.btnSauver = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnServices
            // 
            this.btnServices.Location = new System.Drawing.Point(165, 77);
            this.btnServices.Name = "btnServices";
            this.btnServices.Size = new System.Drawing.Size(75, 23);
            this.btnServices.TabIndex = 0;
            this.btnServices.Text = "Services";
            this.btnServices.UseVisualStyleBackColor = true;
            this.btnServices.Click += new System.EventHandler(this.btnServices_Click);
            // 
            // btnDiplomes
            // 
            this.btnDiplomes.Location = new System.Drawing.Point(165, 142);
            this.btnDiplomes.Name = "btnDiplomes";
            this.btnDiplomes.Size = new System.Drawing.Size(75, 23);
            this.btnDiplomes.TabIndex = 1;
            this.btnDiplomes.Text = "Diplômes";
            this.btnDiplomes.UseVisualStyleBackColor = true;
            this.btnDiplomes.Click += new System.EventHandler(this.btnDiplomes_Click);
            // 
            // btnEmployes
            // 
            this.btnEmployes.Location = new System.Drawing.Point(165, 213);
            this.btnEmployes.Name = "btnEmployes";
            this.btnEmployes.Size = new System.Drawing.Size(75, 23);
            this.btnEmployes.TabIndex = 2;
            this.btnEmployes.Text = "Employés";
            this.btnEmployes.UseVisualStyleBackColor = true;
            this.btnEmployes.Click += new System.EventHandler(this.btnEmployes_Click);
            // 
            // btnSauver
            // 
            this.btnSauver.Location = new System.Drawing.Point(317, 142);
            this.btnSauver.Name = "btnSauver";
            this.btnSauver.Size = new System.Drawing.Size(92, 23);
            this.btnSauver.TabIndex = 3;
            this.btnSauver.Text = "Sauvegarder";
            this.btnSauver.UseVisualStyleBackColor = true;
            this.btnSauver.Click += new System.EventHandler(this.btnSauver_Click);
            // 
            // fm_Gesper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 347);
            this.Controls.Add(this.btnSauver);
            this.Controls.Add(this.btnEmployes);
            this.Controls.Add(this.btnDiplomes);
            this.Controls.Add(this.btnServices);
            this.Name = "fm_Gesper";
            this.Text = "Gestion  du Personnel";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnServices;
        private System.Windows.Forms.Button btnDiplomes;
        private System.Windows.Forms.Button btnEmployes;
        private System.Windows.Forms.Button btnSauver;
    }
}

