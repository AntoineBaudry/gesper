﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GesperLib;
using MySql.Data.MySqlClient;

namespace WF_Gesper
{
    public partial class fm_Gesper : Form
    {
        private Données bd;
        MySqlConnection Cnx = new MySqlConnection();
        public fm_Gesper()
        {
            InitializeComponent();
            bd = new Données();
            Cnx = bd.ConnexionBDD();
            bd.ToutCharger(Cnx);
        }

        private void btnServices_Click(object sender, EventArgs e)
        {
            fm_Services fmServices = new fm_Services(bd,Cnx);
            fmServices.ShowDialog();
        }

        private void btnDiplomes_Click(object sender, EventArgs e)
        {
            fm_Diplomes fmDiplomes = new fm_Diplomes(bd,Cnx);
            fmDiplomes.ShowDialog();
        }

        private void btnEmployes_Click(object sender, EventArgs e)
        {
            fm_Employes fmEmployes = new fm_Employes(bd,Cnx);
            fmEmployes.ShowDialog();
        }

        private void btnSauver_Click(object sender, EventArgs e)
        {
            bd.Sauver(Cnx);
        }
    }
}
