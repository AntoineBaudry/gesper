﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using GesperLib;

namespace LectureBdd
{
    class Program
    {
        static void Main(string[] args)
        {
            

            MySqlConnection Cnx;

            
            GesperLib.Données donnéz = new Données();
            Cnx = donnéz.ConnexionBDD();

            donnéz.ToutCharger(Cnx);
            //donnéz.ChargerServices(Cnx);
            donnéz.AfficherServices();
            Console.WriteLine("----------------------------------------------");
            //donnéz.ChargerDiplomes(Cnx);
            donnéz.AfficherDiplomes();
            Console.WriteLine("------------------------------------------------");
            //donnéz.ChargerEmploye(Cnx);
            donnéz.AfficherEmployes();
            Console.WriteLine("-----------------------------------------------");
            donnéz.ChargerLesEmployesDesServices(Cnx);

            
            donnéz.DéconnexionBDD(Cnx);
            Console.ReadLine();


            


        }
    }
}
