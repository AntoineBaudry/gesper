delimiter |

DROP PROCEDURE IF EXISTS viderBase |
CREATE PROCEDURE viderBase() 
	BEGIN
		delete from posseder ;
		delete from diplome ;
		delete from employe ;
		delete from service ;
		alter table service auto_increment=0 ;	
		alter table employe auto_increment=0 ;
		alter table diplome auto_increment=0 ;
		alter table posseder auto_increment=0 ;
	END |

delimiter ;